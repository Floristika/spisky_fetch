// Завантаження даних з сервера
fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(users => {
        // Отримання вузла списку
        const userList = document.getElementById('userList');

        // Проходження по кожному користувачу
        users.forEach((user, index) => {
            // Створення нового елементу списку
            const listItem = document.createElement('li');

            // Формування рядка інформації
            let userInfo = `Ім'я: ${user.name}, Прізвище: ${user.name}, Пошта: ${user.email}, Сайт: <a href="${user.website}">${user.website}</a>`;

            // Додавання інформації до елементу списку
            listItem.innerHTML = userInfo;

            // Додавання класу для користувачів з парним ID
            if (user.id % 2 === 0) {
                listItem.classList.add('grayBackground');
            }

            // Додавання елементу списку до вузла списку
            userList.appendChild(listItem);
        });
    })
    .catch(error => console.error('Помилка завантаження даних:', error));
